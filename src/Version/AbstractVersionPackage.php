<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Stdlib\Version;

//
use Composer\InstalledVersions;
use Jantia\Standard\Version\VersionPackageInterface;
use Override;
use ReflectionClass;

use function count;
use function explode;
use function implode;
use function in_array;
use function serialize;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class AbstractVersionPackage implements VersionPackageInterface {
	
	/**
	 * @var array|null[]
	 * @since   3.0.0 First time introduced.
	 */
	private array $_packageInfo = ['name' => NULL, 'package' => NULL, 'version' => NULL];
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct() {
		$this->resolvePackageName()->setVersionInfo($this->getVersionInfo($this->getPackageName()));
	}
	
	/**
	 * @param    string    $version
	 *
	 * @return VersionPackageInterface
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function setVersionInfo(string $version) : VersionPackageInterface {
		//
		if(empty($this->_packageInfo['version'])):
			$this->_packageInfo['version'] = $version;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|NULL    $name
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function resolvePackageName(string $name = NULL) : static {
		// If name is not provided, use the namespace name
		if(empty($this->_packageInfo['package'])):
			if(! empty($name)):
				$this->setPackageName($name);
			else:
				$this->_resolvePackageName();
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string|NULL    $name
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function setPackageName(string $name = NULL) : static {
		// If name is not provided, use the namespace name
		if(empty($this->_packageInfo['package']) && $name !== NULL):
			$this->_packageInfo['package'] = $name;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _resolvePackageName() : void {
		//
		$installed = InstalledVersions::getInstalledPackages();
		$ns        = ( new ReflectionClass($this) )->getNamespaceName();
		$array     = explode('\\', $ns);
		
		// Namespace must always have the root name + something after that (Tiat\Something)
		if(( $max = count($array) ) > 1):
			// Get the root name from the namespace name and remove it from the array
			$root = $array[0];
			unset($array[0]);
			$counter = 1;
			
			// Find the package name that matches the namespace name, excluding the root name
			while($counter <= $max):
				// Remove the root name from the array and join it with the remaining elements
				$name = strtolower($root . '/' . implode('-', $array));
				unset($array[$max - $counter]);
				
				// Check if the package name exists in the installed packages
				if(in_array($name, $installed, TRUE) === TRUE):
					$this->setPackageName($name);
					break;
				endif;
				
				// Move to the next possible package name in the array
				$counter++;
			endwhile;
		endif;
	}
	
	/**
	 * @param    string|NULL    $name
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function getVersionInfo(string $name = NULL) : ?string {
		// If version is not provided, use the version from the installed packages
		if(empty($this->_packageInfo['version']) &&
		   ( $version = InstalledVersions::getPrettyVersion($name ?? $this->getPackageName()) ) !== NULL):
			$this->setVersionInfo($version);
		endif;
		
		//
		return $this->_packageInfo['version'] ?? NULL;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function getPackageName() : ?string {
		// If package name is not provided, use the package name from the installed packages
		if(empty($this->_packageInfo['package'])):
			$this->resolvePackageName();
		endif;
		
		//
		return $this->_packageInfo['package'] ?? NULL;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function getResult() : array {
		return $this->toArray();
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function toArray() : array {
		return $this->_packageInfo;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string {
		return $this->_packageInfo['name'] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return VersionPackageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : VersionPackageInterface {
		//
		$this->_packageInfo['name'] = $name;
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function __toString() : string {
		return $this->toString();
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function toString() : string {
		return serialize($this->toArray());
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function __toArray() : array {
		return $this->toArray();
	}
	
	/**
	 * Explode the namespace string and return it without the last part
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _resolveNamespace() : ?string {
		// If the namespace name is not empty, explode it and remove the last element (the class name)
		if(! empty($ns = ( new ReflectionClass($this) )->getNamespaceName())):
			// Define the separator and the array to hold the namespace elements
			$separator = '\\';
			$array     = explode($separator, $ns);
			unset($array[(int)count($array) - 1]);
			
			// Join the remaining elements with the separator and return the result
			return implode($separator, $array);
		endif;
		
		//
		return NULL;
	}
}
