<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Stdlib
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Stdlib\Message;

//
use Jantia\Asi\Helper\AsiHelperInterfaceTrait;
use Jantia\Asi\Register\AsiRegisterHelper;
use Jantia\Standard\Asi\AsiHelperInterface;
use Jantia\Standard\Message\MessageInterface;
use Jantia\Stdlib\Exception\InvalidArgumentException;
use Jantia\Stdlib\Exception\RuntimeException;
use Psr\Log\LogLevel;
use ReflectionClass;
use Tiat\Standard\Time\TimestampInterface;
use Tiat\Stdlib\Time\TimestampTrait;

use function is_array;
use function is_string;
use function register_shutdown_function;
use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractMessage implements AsiHelperInterface, MessageInterface, TimestampInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiHelperInterfaceTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiRegisterHelper;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use TimestampTrait;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_abstractMessages = ['templates' => [], 'messages' => []];
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(...$args) {
		register_shutdown_function([$this, 'shutdown']);
	}
	
	/**
	 * @param    array    $templates
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessageTemplates(array $templates = []) : void {
		//
		if(! empty($templates)):
			// Check if templates are associative array
			foreach($templates as $key => $val):
				if(is_array($val)):
					$tmp                                        = $this->_abstractMessages['templates'][$key] ?? [];
					$this->_abstractMessages['templates'][$key] = [...$tmp, ...$val];
				else:
					throw new InvalidArgumentException("Message values must be an array.");
				endif;
			endforeach;
		endif;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void {
		// This is the backup if Logger have not been found in setMessage()
		if(! empty($messages = $this->getMessages())):
			// Get the system default Logger
			if(( $log = $this->getDefaultLogger() ) !== NULL):
				// Set the Logger to the current instance
				$this->setLogger($log);
				
				// Loop the messages to Log interface
				foreach($messages as $key => $index):
					// Check if key exists
					foreach($index as $key2 => $value):
						// Add message to log
						$log->log($key, $value['message'], (array)$value);
						
						// Remember to remove message
						$this->resetMessage($key, $key2);
					endforeach;
					
					// Reset the main key
					$this->resetMessage($key);
				endforeach;
			else:
				throw new RuntimeException("Can't find the system default Logger.");
			endif;
			
			// Check if there are still messages to be logged
			if($this->countMessages() > 0):
				$msg = sprintf("Please ensure that you will handle messages to your logs on %s.",
				               ( new ReflectionClass($this) )->getName());
				throw new RuntimeException($msg);
			endif;
		endif;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessages() : array {
		return $this->_abstractMessages['messages'];
	}
	
	/**
	 * @param    string|int         $key
	 * @param    string|int|NULL    $index
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetMessage(string|int $key, string|int $index = NULL) : static {
		// Check if key exists
		if($index !== NULL):
			unset($this->_abstractMessages['messages'][$key][$index]);
		else:
			unset($this->_abstractMessages['messages'][$key]);
		endif;
		
		return $this;
	}
	
	/**
	 * @param    null|string    $key
	 *
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function countMessages(string $key = NULL) : int {
		// Check if key exists
		if($key !== NULL):
			return count($this->_abstractMessages['messages'][$key]) ?? 0;
		else:
			return count($this->_abstractMessages['messages']) ?? 0;
		endif;
	}
	
	/**
	 * @param    array    $messages
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessages(array $messages) : static {
		//
		if(! empty($messages)):
			foreach($messages as $key => $message):
				$this->setMessage($key, $message);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    null|string          $key
	 * @param    null|string|array    $message
	 * @param    null|string          $level
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessage(?string $key, string|array|null $message = NULL, string $level = NULL) : void {
		// Add an array as self
		if(is_array($message)):
			$context = $message;
		elseif(is_string($message)):
			// If message is string then add it as description
			$context = ['description' => $message];
		else:
			$context = '';
		endif;
		
		//
		if(( $index = ( $level !== NULL ) ? ( $level ) : ( $this->findMessageLevel($key) ) ) !== FALSE):
			// Get LoggerInterface
			if(( $log = $this->getLog() ) === NULL && ( $log = $this->getDefaultLogger() ) !== NULL):
				$this->setLogger($log);
			endif;
			
			// Use Logger instead of internal array
			if($log instanceof Logger):
				//
				$msg = $this->_abstractMessages['templates'][$index][$key] ?? 'no_message';
				$log->log($index, $msg, (array)$context);
				
				// Remember to remove message
				$this->resetMessage($index, $key);
				
				// Reset the main key
				$this->resetMessage($index);
			else:
				$this->_abstractMessages['templates'][$index][$key] = ['context' => $context];
			endif;
		else:
			$msg = sprintf('Can not find the message template for message with key %s', $key);
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return string|false
	 * @since   3.0.0 First time introduced.
	 */
	public function findMessageLevel(string $key) : string|false {
		// Find all official PSR log levels
		$levels = ( new ReflectionClass(LogLevel::class) )->getConstants();
		
		//
		$key = strtolower($key);
		foreach($levels as $index):
			if(isset($this->_abstractMessages['templates'][$index][$key])):
				return $index;
			endif;
		endforeach;
		
		//
		return FALSE;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetMessages() : static {
		//
		$this->_abstractMessages['messages'] = [];
		
		//
		return $this;
	}
}
