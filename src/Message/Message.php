<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Stdlib\Message;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Message extends AbstractMessage {

}
