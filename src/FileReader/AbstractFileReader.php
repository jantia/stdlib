<?php

/**
 * Jantia
 *
 * @package        Jantia/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Stdlib\FileReader;

//
use Jantia\Stdlib\Exception\InvalidArgumentException;

use function implode;
use function in_array;
use function sprintf;
use function strtoupper;

/**
 * AbstractFileReader is an abstract class that implements the FileReaderInterface.
 * This class serves as a base for creating different types of file readers.
 * Any class that extends AbstractFileReader must implement the abstract methods defined
 * in the FileReaderInterface.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractFileReader implements FileReaderInterface {
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_encoding = 'UTF-8';
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncoding() : string {
		return $this->_encoding;
	}
	
	/**
	 * @param    string    $encoding
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncoding(string $encoding) : static {
		//
		if($this->_checkEncoding($encoding)):
			$this->_encoding = $encoding;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $encoding
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkEncoding(string $encoding) : bool {
		//
		if(! in_array(strtoupper($encoding), self::ACCEPTED_ENCODINGS, TRUE)):
			$msg = sprintf("Encoding is not accepted. Please use one in following list: '%s'",
			               implode(', ', self::ACCEPTED_ENCODINGS));
			throw new InvalidArgumentException($msg);
		endif;
		
		return TRUE;
	}
	
	/**
	 * @param    string    $type
	 *
	 * @return null|string[]
	 * @since   3.0.0 First time introduced.
	 */
	protected function _acceptFileExtension(string $type) : ?array {
		return match ( $type ) {
			'text' => ['txt', 'csv'],
			default => NULL
		};
	}
}