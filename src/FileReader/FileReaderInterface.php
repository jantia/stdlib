<?php

/**
 * Jantia
 *
 * @package        Jantia/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Stdlib\FileReader;

/**
 * An interface to define the basic operations required for reading files.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface FileReaderInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const array ACCEPTED_ENCODINGS = ['ASCII', 'ISO-8859-1', 'UTF-8'];
	
	/**
	 * @param    string    $filename
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function readTextFile(string $filename) : void;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncoding() : string;
	
	/**
	 * Sets the encoding for the file reader.
	 *
	 * @param    string    $encoding    The character encoding to use.
	 *
	 * @return FileReaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncoding(string $encoding) : FileReaderInterface;
}