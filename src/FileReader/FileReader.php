<?php

/**
 * Jantia
 *
 * @package        Jantia/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Stdlib\FileReader;

//
use Jantia\Stdlib\Exception\InvalidArgumentException;

use function file_exists;
use function in_array;
use function is_readable;
use function pathinfo;
use function sprintf;
use function strtolower;

/**
 * This file will read the source file and handle encoding
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class FileReader extends AbstractFileReader {
	
	/**
	 * @param    string    $filename
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function readTextFile(string $filename) : void {
		// If the file exists and is readable, and it's a text file
		if(file_exists($filename) && is_readable($filename)):
			// Check the file extension to ensure it's a text file
			if(in_array(( $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION)) ),
				$this->_acceptFileExtension('text'), TRUE)):
				return;
			else:
				$msg = sprintf("Filename extension (%s) is not supported text file", $ext);
				throw new InvalidArgumentException($msg);
			endif;
		else:
			$msg = sprintf("Filename '%s' doesn't exists or it's not readable.", $filename);
			throw new InvalidArgumentException($msg);
		endif;
	}
	
}
